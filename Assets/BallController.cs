﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

	public GameController _gameController;
	public GameObject HitParticlePrefab;
	public GameObject HitParticlePrefab1;
	public GameObject HitParticlePrefab2;
	private float hitDelay;
	private int prevHitLayer;

	// Use this for initialization
	void Start () 
	{
		_gameController = GameObject.Find("GameController").GetComponent<GameController>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (hitDelay > 0)
		{
			hitDelay -= Time.deltaTime;
		}
	}

	void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.tag == "target")
		{
			collider.SendMessage("SelfDestruct");
		}

//		if (collider.tag == "goal")
//		{
//			collider.SendMessage("HitGoal");
//		}
	}

	void OnCollisionEnter2D (Collision2D collision)
	{
		if (collision.gameObject.tag == "wall")
		{
//			Debug.Log(collision.gameObject.layer);
			if (collision.gameObject.layer == 8)
			{
				if (prevHitLayer != collision.gameObject.layer)
				{
					if (hitDelay <= 0)
					{
						hitDelay = 0.3f;
						Instantiate(HitParticlePrefab, transform.position, Quaternion.identity);
					}
				}
			} else
			{
				if (hitDelay <= 0)
				{
					hitDelay = 0.3f;
					Instantiate(HitParticlePrefab, transform.position, Quaternion.identity);
				}
			}
			prevHitLayer = collision.gameObject.layer;
		}

		if (collision.gameObject.tag == "goal")
		{
			collision.gameObject.SendMessage("HitGoal");
			if (collision.contacts[0].point.x < 120)
			{
				Instantiate(HitParticlePrefab1, transform.position, Quaternion.identity);
			} else
			{
				Instantiate(HitParticlePrefab2, transform.position, Quaternion.identity);
			}
		}

	}
}
