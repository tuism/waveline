﻿using UnityEngine;
using System.Collections;

public class CustomPhysics : MonoBehaviour {

	public Rigidbody2D thisRigidBody;

	// Use this for initialization
	void Start () 
	{
		thisRigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log(thisRigidBody.angularVelocity);
	
	}

	void OnTriggerEnter2D (Collider2D collider2D)
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit))
		{
//			Debug.Log("Point of contact: "+hit.point);
		}

//		thisRigidBody.velocity = (Vector2.up * 19.8f) + collider2D.GetComponent<Rigidbody2D>().velocity;
		thisRigidBody.velocity = Vector2.up * 19.8f;
		thisRigidBody.AddForceAtPosition(collider2D.GetComponent<Rigidbody2D>().velocity, hit.point, ForceMode2D.Force);
//		Debug.Log("hit");
	}
		
}
