﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Rewired;

public class GameController : MonoBehaviour {

	[Header("Prefabs")]
	public GameObject stringSegmentPrefab;
	public GameObject stringControlPrefab;
	public GameObject stringColliderPrefab;

	[Header("Game Settings")]
	public int numberOfSegments;
	public float segmentSize;
	public float yLimit;

	[Header("Score variables")]
	public int[] playerScores;
	public Text[] playerScoreObjects;
	private bool gameOver;

	[Header("Control Object")]
	public GameObject ControlledObject1;
	public Rigidbody2D ControlledRigid1;
	public GameObject ControlledObject2;
	public Rigidbody2D ControlledRigid2;
	public Transform Player1Object;
	public Transform Player2Object;
	public Transform Player1Object2;
	public Transform Player2Object2;
	public Rigidbody2D Ball;

	[Header("Level Settings")]
	public GameObject[] Levels;
	public int[] levelNumberOfObjects;
	public int thisLevelObjects;
	public int currentLevel;

	[Header("String variables")]

	public LineRenderer stringLineRenderer;
	private Rigidbody2D prevRopeBody;

	private List<Rigidbody2D> stringColliders = new List<Rigidbody2D>();
	private Vector3[] stringSegmentPositions;
	private List<GameObject> stringSegments = new List<GameObject>();

	[Header("String du deux")]

	public LineRenderer stringLineRenderer2;
	private Rigidbody2D prevRopeBody2;

	private List<Rigidbody2D> stringColliders2 = new List<Rigidbody2D>();
	public float topStringOffset;
	private Vector3[] stringSegmentPositions2;


	[Header("Camera variables")]
	private Vector3 startingCamPos;
	public List<Transform> trackedObjects = new List<Transform>();

	[Header("Touch variables")]
	public Image[] TouchPoints;
	public bool isTouch;

	[Header("Controller stuff")]
	public int playerId = 0; // The Rewired player id of this character
	private Player player; // The Rewired Player
	public Vector2 leftAxis;
	public Vector2 rightAxis;
	public bool restartButton;

	[Header("GUI")]
	public GameObject[] WinnerGUI;
	public GameObject RestartButton;

	[Header("Sound Stuff")]
	public AudioSource SoundSource;
	public AudioClip[] SoundClips;
	private float beatTimer;
	public float beatTime;
	public bool playHit1;
	public bool playHit2;


	void Awake() 
	{
		// Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
		player = ReInput.players.GetPlayer(playerId);
	}


	// Use this for initialization
	void Start () 
	{
		Ball = GameObject.Find("BALL").GetComponent<Rigidbody2D>();
		gameOver = false;
//		WinnerGUI = new GameObject[2];
		isTouch = false;
//		TouchPoints = new Transform[2];
		playerScores = new int[2];
		playerScores[0] = 0;
		playerScores[1] = 0;
		stringSegmentPositions = new Vector3[numberOfSegments];
		stringSegmentPositions2 = new Vector3[numberOfSegments];

		Time.timeScale = 1f;

		// string 1

		stringLineRenderer = GetComponent<LineRenderer>();
		stringLineRenderer.SetPositions(stringSegmentPositions);
		stringLineRenderer.SetColors(Color.white, Color.white);
		stringLineRenderer.SetWidth(3f,3f);

		// string 2

		stringLineRenderer2 = GameObject.Find("Line2").GetComponent<LineRenderer>();
		stringLineRenderer2.SetPositions(stringSegmentPositions2);
		stringLineRenderer.SetColors(Color.white, Color.white);
		stringLineRenderer2.SetWidth(3f,3f);

		//

		currentLevel = 0;
		thisLevelObjects = levelNumberOfObjects[currentLevel];

		//make left control object

		HingeJoint2D tempHinge0;

		GameObject tempRopeStartSegment = Instantiate(stringControlPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity) as GameObject;
		tempHinge0 = tempRopeStartSegment.GetComponent<HingeJoint2D>();
		tempHinge0.enabled = false;

		ControlledObject1 = tempRopeStartSegment;
		ControlledRigid1 = ControlledObject1.GetComponent<Rigidbody2D>();

		//

//		GameObject tempRopeSegment = Instantiate(stringSegmentPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
//		GameObject tempRopeCollider = Instantiate(stringColliderPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
//		stringSegments.Add(tempRopeSegment);
//		stringColliders.Add(tempRopeCollider.GetComponent<Rigidbody2D>());

		for (int i = 0; i < numberOfSegments; i++)
		{
			//string 1
			HingeJoint2D tempHinge;
			GameObject tempRopeSegment = Instantiate(stringSegmentPrefab, new Vector3(i * segmentSize, 0, 0), Quaternion.identity) as GameObject;
			GameObject tempRopeCollider = Instantiate(stringColliderPrefab, new Vector3(i * segmentSize, 0, 0), Quaternion.identity) as GameObject;

			stringSegments.Add(tempRopeSegment);
			stringColliders.Add(tempRopeCollider.GetComponent<Rigidbody2D>());

			tempHinge = tempRopeSegment.GetComponent<HingeJoint2D>();
			if (i == 0)
			{
				tempHinge.connectedBody = ControlledRigid1;
				prevRopeBody = tempRopeSegment.GetComponent<Rigidbody2D>();
			} else
			{
				tempHinge.connectedBody = prevRopeBody;
				prevRopeBody = tempRopeSegment.GetComponent<Rigidbody2D>();
			}

			//string 2

//			HingeJoint2D tempHinge;
//			tempRopeSegment = Instantiate(stringSegmentPrefab, new Vector3(i * segmentSize, topStringOffset, 0), Quaternion.identity) as GameObject;
			tempRopeCollider = Instantiate(stringColliderPrefab, new Vector3(i * segmentSize, topStringOffset, 0), Quaternion.identity) as GameObject;

//			stringSegments2.Add(tempRopeSegment);
			stringColliders2.Add(tempRopeCollider.GetComponent<Rigidbody2D>());

//			tempHinge = tempRopeSegment.GetComponent<HingeJoint2D>();
//			if (i == 0)
//			{
//				tempHinge.connectedBody = ControlledRigid1;
//				prevRopeBody = tempRopeSegment.GetComponent<Rigidbody2D>();
//			} else
//			{
//				tempHinge.connectedBody = prevRopeBody;
//				prevRopeBody = tempRopeSegment.GetComponent<Rigidbody2D>();
//			}

		}


		HingeJoint2D tempHinge2;

		//make right control object

		GameObject tempRopeEndSegment = Instantiate(stringControlPrefab, new Vector3(numberOfSegments * segmentSize, 0, 0), Quaternion.identity) as GameObject;
		tempHinge2 = tempRopeEndSegment.GetComponent<HingeJoint2D>();
		tempHinge2.connectedBody = prevRopeBody;

		ControlledObject2 = tempRopeEndSegment;
		ControlledRigid2 = ControlledObject2.GetComponent<Rigidbody2D>();

		//

		Camera.main.transform.position = new Vector3(numberOfSegments,50f,-2f);
		startingCamPos = Camera.main.transform.position;
	}
	
	// Update is called once per frame

	void FixedUpdate ()
	{
		//camera controls
		if (trackedObjects[0].position.y > 160)
		{
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(startingCamPos.x, (trackedObjects[0].position.y/2)+50f, startingCamPos.z),0.1f);
			Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, trackedObjects[0].position.y/1.1f, 0.1f);
		} else
		{
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, startingCamPos,0.1f);
			Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, 140, 0.1f);
		}

		if (Time.timeScale < 1f)
		{
//			Time.timeScale = Time.timeScale += Time.deltaTime/10f;
			Time.timeScale = Mathf.Lerp(Time.timeScale, 1f, 0.009f);

			if (Time.timeScale >0.3f)
			{
				Time.timeScale += 0.05f;
			}

			if (Time.timeScale > 1f)
			{
				Time.timeScale = 1f;
			}
			Time.fixedDeltaTime = 0.02F * Time.timeScale;
		}
	}

	void GetInput()
	{
		// Get the input from the Rewired Player. All controllers that the Player owns will contribute, so it doesn't matter
		// whether the input is coming from a joystick, the keyboard, mouse, or a custom controller.

//		Debug.Log(player);
//		Debug.Log(player.GetAxis(0));
		leftAxis.y = player.GetAxis(0); // get input by name or action id
		rightAxis.y = player.GetAxis(1);
		restartButton = player.GetButton(2);
//		fire = player.GetButtonDown("Fire");

	}

	void Update () 
	{

		//sound
		beatTimer += Time.deltaTime;

		if (beatTimer > beatTime)
		{
			SoundSource.PlayOneShot(SoundClips[0]);
			if (playHit1)
			{
				SoundSource.PlayOneShot(SoundClips[1]);
				playHit1 = false;
			}
			if (playHit2)
			{
				SoundSource.PlayOneShot(SoundClips[2]);
				playHit2 = false;
			}
			beatTimer -= beatTime;
		}

		//

		// controller controls

//		Debug.Log(ReInput.players.playerCount);

//		if (gameOver)
//		{
			if (restartButton)
			{
				playerScores[0] = 0;
				playerScores[1] = 0;
				WinnerGUI[0].SetActive(false);
				WinnerGUI[1].SetActive(false);
				RestartButton.SetActive(false);
				playerScoreObjects[0].text = playerScores[0].ToString();
				playerScoreObjects[1].text = playerScores[1].ToString();
				Ball.transform.position = new Vector3(120,110,0);
				Ball.velocity = Vector3.zero;
			}
//		}
		if (ReInput.controllers.joystickCount > 0)
		{
			GetInput();
			ControlledRigid1.MovePosition( new Vector2(0f, leftAxis.y * yLimit));
			ControlledRigid2.MovePosition( new Vector2(numberOfSegments * 2f, rightAxis.y * yLimit));
		} else

		// end controller controls


		//touch controls

		if (Input.touchCount > 0)
		{
			if (!isTouch) isTouch = true;
			for (int i = 0; i < Input.touchCount; i++)
			{
				if (i < 2)
				{
					Vector2 testTouchPoint = Input.GetTouch(i).position;
					if (testTouchPoint.x < Camera.main.pixelWidth/2)
					{
						float Ytarget1 = Camera.main.ScreenToWorldPoint(testTouchPoint).y;
						if (Ytarget1 > yLimit)
						{
							Ytarget1 = yLimit;
						}
						if (Ytarget1 < -yLimit)
						{
							Ytarget1 = -yLimit;
						}
						ControlledRigid1.MovePosition( new Vector2(0f, Ytarget1));

						TouchPoints[0].rectTransform.anchoredPosition = new Vector2 (testTouchPoint.x, testTouchPoint.y);
					} else
					{
						float Ytarget1 = Camera.main.ScreenToWorldPoint(testTouchPoint).y;
						if (Ytarget1 > yLimit)
						{
							Ytarget1 = yLimit;
						}
						if (Ytarget1 < -yLimit)
						{
							Ytarget1 = -yLimit;
						}
						ControlledRigid2.MovePosition( new Vector2(numberOfSegments*2f, Ytarget1));

						TouchPoints[1].rectTransform.anchoredPosition = new Vector2 (testTouchPoint.x, testTouchPoint.y);
					}
				}
			}
		} else if (!isTouch)
		//end touch controls
		{
			float Ytarget = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
			if (Ytarget > yLimit)
			{
				Ytarget = yLimit;
			}
			if (Ytarget < -yLimit)
			{
				Ytarget = -yLimit;
			}
			ControlledRigid1.MovePosition( new Vector2(0, 0));
			ControlledRigid2.MovePosition( new Vector2(numberOfSegments*2f, Ytarget));
		}

		Player1Object.position = ControlledRigid1.transform.position;
		Player2Object.position = ControlledRigid2.transform.position;
		Player1Object2.position = ControlledRigid1.transform.position + (Vector3.up * topStringOffset);
		Player2Object2.position = ControlledRigid2.transform.position + (Vector3.up * topStringOffset);

		for (int i = 0; i < numberOfSegments; i++)
		{
			stringSegmentPositions[i] = stringSegments[i].transform.position;
			stringSegmentPositions2[i] = stringSegments[i].transform.position + (Vector3.up * topStringOffset);
			stringColliders[i].MovePosition(stringSegments[i].transform.position);
			stringColliders[i].MoveRotation(stringSegments[i].transform.eulerAngles.z);

			stringColliders2[i].MovePosition(stringSegments[i].transform.position + (Vector3.up * topStringOffset));
			stringColliders2[i].MoveRotation(stringSegments[i].transform.eulerAngles.z);

		}
		stringLineRenderer.SetPositions(stringSegmentPositions);
		stringLineRenderer2.SetPositions(stringSegmentPositions2);

		stringLineRenderer.SetPosition(120,stringSegmentPositions[119] + (Vector3.right*5f));
		stringLineRenderer2.SetPosition(120,stringSegmentPositions[119] + (Vector3.up * topStringOffset) + (Vector3.right*5f));

		//check for next level

		if (thisLevelObjects == 0)
		{
			if (currentLevel < Levels.Length-1)
			{
				currentLevel += 1;
				thisLevelObjects = levelNumberOfObjects[currentLevel];
				Levels[currentLevel-1].SetActive(false);
				Levels[currentLevel].SetActive(true);
			}
		}

	}

	public void AddScore(int player, int amountToAdd)
	{
		if (!gameOver)
		{
			playerScores[player] += amountToAdd;
			if (player == 0)
			{
				playHit1 = true;
			} else
			if (player == 1)
			{
				playHit2 = true;
			}

			playerScoreObjects[player].text = playerScores[player].ToString();
			for (int i = 0; i < 2; i++)
			{
				if (playerScores[i] >= 13)
				{
					WinnerGUI[i].SetActive(true);
					RestartButton.SetActive(true);
					gameOver = true;
				}
			}
		}
	}

	public void SlowTime()
	{
		Time.timeScale = 0.005f;
		Time.fixedDeltaTime = 0.02F * Time.timeScale;
	}
}
