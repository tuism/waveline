﻿using UnityEngine;
using System.Collections;

public class Limiter : MonoBehaviour 
{
	private Rigidbody2D thisRigid;
	public float maxVel;

	// Use this for initialization
	void Start () {
		thisRigid = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (thisRigid.velocity.magnitude > maxVel)
		{
			thisRigid.velocity = thisRigid.velocity * maxVel / thisRigid.velocity.magnitude;
		}
	}
}
