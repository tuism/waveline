﻿using UnityEngine;
using System.Collections;

public class ScoreGate : MonoBehaviour {

	public int playerScoreFor;
	public int addScore;
	private GameController _gameController;

	// Use this for initialization
	void Start () 
	{
		_gameController = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void HitGoal ()
	{
		_gameController.SlowTime();
		_gameController.AddScore(playerScoreFor, addScore);
	}
}
