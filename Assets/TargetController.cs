﻿using UnityEngine;
using System.Collections;

public class TargetController : MonoBehaviour {

	public GameController _gameController;

	// Use this for initialization
	void Start () 
	{
		_gameController = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void SelfDestruct ()
	{
		_gameController.thisLevelObjects -= 1;
		Destroy(gameObject);
	}
}
